#include <vector>
#include <iostream>
using std::vector;
using std::cout;

int main()
{
	vector<int> v1;
	for(int i = 1; i < 100000; i++){
		v1.push_back(i);
	}
	for(vector<int>::iterator iter = v1.begin(); iter != v1.end(); iter++){
		if(*iter%2 == 0)
			cout << *iter << "\n";
	}
}
