#include <iostream>

void change(int &a, int &b)
{
	int tmp;
	tmp = a;
	a = b;
	b = tmp;
}

int main()
{
	int a = 10, b = 20;
	change(a, b);
	std::cout << a << "\n" << b << "\n";
}
