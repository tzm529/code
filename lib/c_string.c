#include "c_string.h"

static inline void *_char_malloc(int len);

cstring *cstring_create(int len)
{
	cstring *str = malloc(sizeof(struct _cstring));
	str->value = _char_malloc(len);
	str->length = len;
	return str;
}
/*************************************************************/
static inline void *_char_malloc(int len)
{
	char *p = malloc(len);
	assert(p != NULL);	
	return p;
}

#ifdef DEBUG
int main()
{
	cstring *s = cstring_create(100);
	s->value = "测试";
	printf("%s", s->value);
}
#endif
