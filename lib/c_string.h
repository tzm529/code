#ifndef C_STRING
#define C_STRING

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <assert.h> 

struct _cstring{
	void *value;
	unsigned int length;
};
typedef struct _cstring cstring;

cstring *cstring_create(int len);

#endif
