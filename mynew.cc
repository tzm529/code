#include <iostream>

int main()
{
	int len = 10, i = 0;
	int *p = new int[len]();
	int *q = NULL;
	for(q = p, i = 0; q!=(p+len); q++, i++){
		*q = i+ 10;
	}
	for(q = p; q!=(p+len); q++){
		std::cout << *q << "\n";
	}
	delete [] p;
}
